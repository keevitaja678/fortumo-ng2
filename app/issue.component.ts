import { Component } from '@angular/core';
import { Issue } from './issue';

const ISSUES: Issue[] = [
  { id: 11, title: 'I received a reply message saying �Enter this
	code in payment window to complete the
	transaction: XXXXXX�. What should I do?' },
  { id: 12, title: 'Did I get subscribed to a service? ' },
  { id: 13, title: 'What are the spending limits for Fortumo services?' },
  { id: 14, title: 'What is Fortumo?' }
];

@Component({
    selector: 'fortumo-issue',
    template: `
	<li *ngFor="let issue of issues" class="issue">
	  <h3 class="issue__title">{{issue.title}}</h3>
	  <img class="issue__arrow_right" src="logo.svg">
	</li>
	`
})
export class IssueComponent { 
  issues = ISSUES;
}
