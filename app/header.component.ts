import { Component } from '@angular/core';

@Component({
    selector: 'fortumo-header',
    template: `
	<header>
	  <img src="logo.svg">
	  <span class="header__page-title">{{pagetitle}}</span>
	  <span class="header__language-selector"><img src="language.svg">{{currentlanguage}}</span>
	</header>`
})
export class HeaderComponent {
	pagetitle = 'Customer support';
	currentlanguage = 'English';
}
