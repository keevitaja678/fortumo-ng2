import { Component } from '@angular/core';
import { Issue } from './issue';

const POPULARISSUES: Issue[] = [
  { id: 11, title: 'I haven�t received the service' },
  { id: 12, title: 'I'm not able to make a payment' }
];

@Component({
    selector: 'fortumo-popularissue',
    template: `
	<li *ngFor="let popularissue of popularissues" class="popularissue">
	  <h3 class="popularissue__title">{{popularissue.title}}</h3>
	  <img class="popularissue__arrow_right" src="logo.svg">
	</li>
	`
})
export class PopularissueComponent { 
  popularissues = POPULARISSUES;
}
