import { Component, Input } from '@angular/core';

@Component({
    selector: 'fortumo-search',
    template: `
	<li class="issue-search">
		<h3 class="issue-search__title">{{searchtitle}}</h3>
		<input type="text" name="issue-search__input" class="issue-search__input">
		<input type="submit" name="issue-search__submit" class="issue-search__submit" placeholder="e.g. How to make payment">
	</li>`
	`
})
export class SearchComponent { 
  searchtitle = 'I would like to get more info:';
}
